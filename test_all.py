from unittest import TestLoader, TestSuite, TextTestRunner

if __name__ == '__main__':
    loader = TestLoader()
    discovered_suite = loader.discover('test')
    test_suite = TestSuite(discovered_suite)
    runner = TextTestRunner(verbosity=2)
    result = runner.run(test_suite)

    if result.wasSuccessful():
        exit(0)
    else:
        exit(1)
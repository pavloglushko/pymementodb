## [1.0.1](https://gitlab.com/pavloglushko/pymementodb/compare/v1.0.0...v1.0.1) (2022-12-13)


### Bug Fixes

* **infrastructure:** fix download url in version.py file ([910cc16](https://gitlab.com/pavloglushko/pymementodb/commit/910cc163ab760b3d0c26c46fef98765bebadbe65))
* **release:** fix expected match not found in release ([90dfc8b](https://gitlab.com/pavloglushko/pymementodb/commit/90dfc8b2fa6130c91415fd8f1546d693debe65df))

# 1.0.0 (2022-12-02)


### Features

* implement all classes and methods ([93942c5](https://gitlab.com/pavloglushko/pymementodb/commit/93942c53dbee532b35420c8e4781234c8dd4b7fa))


### BREAKING CHANGES

* First working version.
